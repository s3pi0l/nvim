return {
	"stevearc/oil.nvim",
	opts = {},
	config = function()
		require("oil").setup({
			default_file_explorer = true,
			delete_to_trash = true,
			columns = {
				"icon",
				"permissions",
				"size",
			},
			view_options = {
				show_hidden = true,
				is_hidden_file = function(name, bufnr)
					return vim.startswith(name, ".")
				end,
				is_always_hidden = function(name, bufnr)
					return false
				end,
				natural_order = true,
				sort = {
					{ "type", "asc" },
					{ "name", "asc" },
				},
			},

			vim.keymap.set("n", "<C-m>", ":Oil<CR>", {}),
		})
	end,
}
